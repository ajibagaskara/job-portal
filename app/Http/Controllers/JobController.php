<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Job;

class JobController extends Controller
{
    //index
    public function index(){
        $jobs = Job::all();
        return view('welcome', compact('jobs'));
    }

    //show
    public function show($id, Job $job){
        return view('jobs.show', compact('job'));
    }
}
