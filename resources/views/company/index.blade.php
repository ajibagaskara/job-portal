@extends('layouts.app')

@section('content')
<div class="container">
    <div class="col-md-12">
        <div class="company-profile">
            <img src="{{ asset('img/cover/tumblr-image-sizes-banner.png') }}" alt="Banner Image" style="width:100%;">
            <div class="company-desc">
                <img src="{{ asset('img/avatar/serwman1.jpg') }}" alt="Desc-Img" width="100;">
                <p>{{$company->desc}}</p>
                <h1>{{$company->company_nm}}</h1>
                <p>Slogan-{{$company->slogan}}&nbsp;Address-{{$company->address}}&nbsp;Phone-{{$company->phone}}&nbsp;Website-{{$company->website}}</p>
            </div>
        </div>
        <table class="table">
            <thead>
                <th></th>
                <th></th>
                <th></th>
                <th></th>
                <th></th>
            </thead>
            <tbody>
                @foreach ($company->jobs as $job)
                <tr>
                    <td>
                        <img src="{{ asset('img/avatar/man.jpg') }}" alt="Avatar Picture"
                        width="80">
                    </td>
                    <td>Position : {{$job->position}}
                        <br>
                        <i class="fa fa-clock-o"></i> {{ $job->type }}
                    </td>
                    <td><i class="fas fa-map-marker"></i> Address : {{ $job->address }}</td>
                    <td>
                        <i class="far fa-calendar-minus"></i> Date : {{ $job->created_at->diffForHumans() }}</td>
                    <td>
                        <a href="{{ route('jobs.show', [$job->id, $job->slug]) }}">
                            <button class="btn btn-info btn-sm">Info</button>
                        </a>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>

    </div><!--col-md-12-->
</div><!--container-->
@endsection
